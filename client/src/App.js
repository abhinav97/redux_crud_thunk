import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AddUser from './Components/AddUser';
import EditUser from './Components/EditUser';
import Home from './Components/Home';

function App() {
  return (
    <div>
      <Router>
        <Routes>
          <Route exact path='/' element={<Home/>}/>
          <Route exact path='/add' element={<AddUser/>}/>
          <Route exact path='/edit/:id' element={<EditUser/>}/>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
