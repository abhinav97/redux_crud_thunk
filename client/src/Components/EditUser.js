import React, {useEffect, useState} from 'react'
import { FormControl, FormGroup, Input, InputLabel, Typography, styled, Button} from '@mui/material';
import {useNavigate, useParams} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import { addUser, getOneUser, updateUser } from '../Redux/Actions';


const Container=styled(FormGroup)`
width: 50%;
margin: 5% auto 0 auto;
& > div {
    margin-top: 25px;
}
`;

const EditUser = () => {

  let {id} = useParams();

  const dispatch = useDispatch();

  const { user } = useSelector((state) => state.userReducer);

  const navigate = useNavigate();

  const [users, setUsers]=useState({
    name: '',
    email : '',
    contact : '',
    city : ''
  }); 

  const {name, email, contact, city} = users;


  const [err, setErr] = useState("");

  useEffect(()=> {
    dispatch(getOneUser(id));
  }, []);

  useEffect(()=> {
    if(user){
      setUsers({ ...user });
    }
  }, [user]);

const onValueChange = (e)=> {
  setUsers({...users, [e.target.name ]: e.target.value})
};

const handleSubmit = ()=> {
  if(!name || !city || !email || !contact){
    setErr("Please input all data");
  }
  else{
    dispatch(updateUser(users, id));
    navigate('/');
    setErr("");
  }
};

  return (
    <div>
    <Container>
        <Typography variant='h5'>Edit User</Typography>
        {err && <h3 style={{color : 'red', textAlign: 'center'}}>{err}</h3>}
        <FormControl>
            <InputLabel>Name</InputLabel>
            <Input onChange={(e)=> onValueChange(e)}  name="name" value={name || ""}/>
        </FormControl>
        <FormControl>
            <InputLabel>Email</InputLabel>
            <Input onChange={(e)=> onValueChange(e)}  name="email" value={email || ""}/>
        </FormControl>
        <FormControl>
            <InputLabel>Contact</InputLabel>
            <Input onChange={(e)=> onValueChange(e)} name="contact" value={contact || ""}/>
        </FormControl>
        <FormControl>
            <InputLabel>City</InputLabel>
            <Input onChange={(e)=> onValueChange(e)}  name="city" value={city || ""}/>
        </FormControl>
        <FormControl>
        <Button variant="contained" onClick={(e)=>handleSubmit(e)}>Update Data</Button>
        </FormControl>
    </Container>

</div>
  )
}

export default EditUser