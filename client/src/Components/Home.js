import React, {useEffect} from 'react';
import {Table, TableHead, TableRow, TableBody, TableCell, Button, styled} from '@mui/material';
import {useDispatch, useSelector} from 'react-redux'
import {loadUsers, deleteUser} from '../Redux/Actions';
import {useNavigate} from 'react-router-dom';


const StyledTable= styled(Table)`
width : 90%;
margin :  50px auto 0 auto;
`;

const THead= styled(TableRow)`
background : #000000;
& > th {
  color : #fff;
  font-size : 20px;
}
`;

const Home = () => {

  const navigate = useNavigate();

  const dispatch = useDispatch();

  const { users } = useSelector(state=>state.userReducer)

  useEffect(()=> {
    dispatch(loadUsers());
  }, []);

  const handleDelete = (id)=> {
    if(window.confirm("Are you sure, you want to delete this user?")){
      dispatch(deleteUser(id));
    }
  }

  return (
    <div>
    <h1 style={{textAlign:'center'}}>Home</h1>
    <Button variant='contained'style={{marginLeft: 620}} onClick={()=>navigate('/add')}>Add User</Button>

    <StyledTable>
        <TableHead>
          <THead>
            <TableCell>Id</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Contact</TableCell>
            <TableCell>City</TableCell>
            <TableCell>Operation</TableCell>

          </THead>

        </TableHead>
        <TableBody>
          {
            users && users.map((user, id) => (
               <TableRow key={user.id}>
                <TableCell>{id + 1}</TableCell>
                <TableCell>{user.name}</TableCell>
                <TableCell>{user.email}</TableCell>
                <TableCell>{user.contact}</TableCell>
                <TableCell>{user.city}</TableCell>
                <TableCell>
                  <Button variant='contained' style={{marginRight : 10}} onClick={()=> navigate(`edit/${user.id}`)}>Edit</Button>
                  <Button variant='contained' onClick={()=> handleDelete(user.id)}>Delete</Button>
                </TableCell>
              </TableRow>
             )) 
          }

        </TableBody>
      </StyledTable>

    </div>
  )
}

export default Home