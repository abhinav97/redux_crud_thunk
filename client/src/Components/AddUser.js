import React, {useState} from 'react'
import { FormControl, FormGroup, Input, InputLabel, Typography, styled, Button} from '@mui/material';
import {useNavigate} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import { addUser } from '../Redux/Actions';

const Container=styled(FormGroup)`
width: 50%;
margin: 5% auto 0 auto;
& > div {
    margin-top: 25px;
}
`;

const AddUser = () => {

  const dispatch = useDispatch();

  const navigate = useNavigate();

  const [user, setUser]=useState({
    name: '',
    email : '',
    contact : '',
    city : ''
  }); 

  const {name, email, contact, city} = user;

  const [err, setErr] = useState("");

const onValueChange = (e)=> {
  setUser({...user, [e.target.name ]: e.target.value})
}

const handleSubmit = ()=> {
  if(!name || !city || !email || !contact){
    setErr("Please input all data");
  }
  else{
    dispatch(addUser(user));
    navigate('/');
    setErr("");
  }
}

  return (
    <div>
    <Container>
        <Typography variant='h5'>Add User</Typography>
        {err && <h3 style={{color : 'red', textAlign: 'center'}}>{err}</h3>}
        <FormControl>
            <InputLabel>Name</InputLabel>
            <Input onChange={(e)=> onValueChange(e)}  name="name" value={name}/>
        </FormControl>
        <FormControl>
            <InputLabel>Email</InputLabel>
            <Input onChange={(e)=> onValueChange(e)}  name="email" value={email}/>
        </FormControl>
        <FormControl>
            <InputLabel>Contact</InputLabel>
            <Input onChange={(e)=> onValueChange(e)} name="contact" value={contact}/>
        </FormControl>
        <FormControl>
            <InputLabel>Address</InputLabel>
            <Input onChange={(e)=> onValueChange(e)}  name="city" value={city}/>
        </FormControl>
        <FormControl>
        <Button variant="contained" onClick={(e)=>handleSubmit(e)}>Add User</Button>
        </FormControl>
    </Container>

</div>
  )
}

export default AddUser