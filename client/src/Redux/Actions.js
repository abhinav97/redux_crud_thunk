import *  as types from './ActionType';
import axios from 'axios'

let URL='http://localhost:5000/user';

const getUsers = (users)=> ({
    type : types.GET_USERS,
    payload : users
})

const deleteData = ()=> ({
    type : types.DELETE_USER
})

const addData = ()=> ({
    type : types.ADD_USER
})

const getOneData = (user)=> ({
    type : types.GET_USER,
    payload : user,
});

const updateData = ()=> ({
    type : types.UPDATE_USER
})


export const loadUsers = ()=> {

    return function (dispatch){
        axios.get(`${URL}`).then((res)=>{
            console.log(res);
            dispatch(getUsers(res.data))
        }).catch((error)=> console.log(error));
    }
}


export const deleteUser = (id)=> {

    return function (dispatch){
        axios.delete(`${URL}/${id}`).then((res)=>{
            console.log(res);
            dispatch(deleteData());
            dispatch(loadUsers());
        }).catch((error)=> console.log(error));
    }
}


export const addUser = (user)=> {

    return function (dispatch){
        axios.post(`${URL}`, user).then((res)=>{
            console.log(res);
            dispatch(addData());
            dispatch(loadUsers());
        }).catch((error)=> console.log(error));
    }
}

export const getOneUser = (id)=> {

    return function (dispatch){
        axios.get(`${URL}/${id}`).then((res)=>{
            console.log(res);
            dispatch(getOneData(res.data));
        }).catch((error)=> console.log(error));
    }
}


export const updateUser = (user, id)=> {

    return function (dispatch){
        axios.put(`${URL}/${id}`, user).then((res)=>{
            console.log(res);
            dispatch(updateData());
        }).catch((error)=> console.log(error));
    }
}